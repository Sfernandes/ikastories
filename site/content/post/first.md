---
title: "First"
date: 2018-05-30T10:49:14+01:00
draft: false
---

<p>“Peace of mind produces right values, right values produce right thoughts. Right thoughts produce right actions and right actions produce work which will be a material reflection for others to see of the serenity at the center of it all.”</p>
<small>― Robert M. Pirsig, Zen and the Art of Motorcycle Maintenance</small>
